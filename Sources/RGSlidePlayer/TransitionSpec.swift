//
//  File.swift
//  
//
//  Created by paul on 03/11/2020.
//

import Foundation
import SwiftUI

public enum TransitionType {
    case dissolve
    case slide
    case foliobook
}

public struct TransitionSpec {
    let transitionType: TransitionType
    let transitionDuration: TimeInterval
    let interTransitionDelay: TimeInterval
}
