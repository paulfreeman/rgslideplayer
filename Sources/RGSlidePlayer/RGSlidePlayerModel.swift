//
//  File.swift
//  
//
//  Created by paul on 03/11/2020.
//

import Foundation
import SwiftUI

public enum SlideplayerState {
    case  stopped
    case  initialised
    case  running
}

public protocol SlidePlayerModelDelegate {
    func getNextImage() //trigger next iteration
}

///Extension to handle views instead of Images left for a future exercise
///complex and not required here
public class RGSlidePlayerModel : ObservableObject {
     
    //MARK:- Handling the views to be animated
    
    ///Top view does all the work
    ///If the view is provided before the duration of the transition has completed
    ///setting the view will have no effect, its the containers responsibility to provide
    ///views at the correct rate.
    @Published public var topView : String = ""
    
    var playerDelegate:SlidePlayerModelDelegate?
    
    public init(view:String?, delegate:SlidePlayerModelDelegate?){
        if let v = view {
          topView = v
        }
        if let d = delegate {
            playerDelegate = delegate
        }
    }
    
    @Published var topViewShowing = false
      
    //Base view provides continuity
    @Published var baseView : String = ""
    
    
    //MARK:- Transition and state
    
    @Published var transitionSpec = TransitionSpec(transitionType: .dissolve,
                                                   transitionDuration:1.0,
                                                   interTransitionDelay: 3.0)
     
    var state:SlideplayerState = .stopped
     
    //MARK:- Handle view updates (initial state)
    
    //MARK:- external API
    public func toggleSlideshowPlaying() {
        if self.state == .stopped {
            self.state = .initialised
        }
        else {
            self.state = .stopped
        }
        performTransition()
    }
    
    func updateState(){
        if self.state == .initialised {
            performTransition()
        }
    }
     
//    func updateState(firstViewShown: Bool){
//        if firstViewShown {
//          state = .initialised
//          baseView = topView
//          topViewShowing = false
//        }
//        else if state == .running || state == .stopped { //ignore any updates while transitions are in progress or player is stopped
//            return
//        }
//        else {
//          performTransition()
//        }
//    }
    
    //MARK:- Player state transitions
    
    func performTransition(){
        switch state {
            case .initialised   : startTransition()
            case .running       : prepareForNextCycle()
            default : break
        }
    }
    
    func startTransition() {
        state = .running
        topViewShowing = true
        performTransition()
    }
    
    func prepareForNextCycle() {
        if state == .stopped { return }
        baseView = topView
        state = .initialised
        topViewShowing = false
        DispatchQueue.main.asyncAfter(deadline: .now() + transitionSpec.interTransitionDelay) {
            self.playerDelegate?.getNextImage()
            self.performTransition()
        }
    }
    
    //MARK:- Public API
    
    public func setTransitionSpec(_ spec: TransitionSpec) {
        self.transitionSpec = spec
    }
    
    public func setNext(image: String) {
        self.topView = image
    }
    
    ///This will prevent further animations but will
    ///not cancel the current animation
    public func stop() {
        state = .stopped
    }
    
    
}
