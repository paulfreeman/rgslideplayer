//
//  ViewHolder.swift
//  
//
//  Created by paul on 03/11/2020.
//

import Foundation
import SwiftUI

struct ViewHolderProperties {
    let borderWidth:CGFloat
    let borderColor:Color
}

///ViewHolder can be larger than the width of the containing ZStack
///to enable a bordered slide transition if its width is > the containing stack
struct ViewHolder <Content: View>: View {
    @State var properties = ViewHolderProperties(borderWidth:0.0, borderColor:.black)
    let viewBuilder: () -> Content
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                HStack {
                    Spacer(minLength:properties.borderWidth)
                    viewBuilder()
                    Spacer(minLength:properties.borderWidth)
                }.background(properties.borderColor)
//To add borders we need to work out how to dynamically change the alignment, ignore for now.
//                .frame(width: geometry.size.width + (properties.borderWidth*2), height: geometry.size.height)
//                .alignmentGuide(HorizontalAlignment.center, computeValue: { dimension in
//                    -properties.borderWidth
//                })
            }
        }
    }
}

struct ViewHolder_Previews: PreviewProvider {
    static var previews: some View {
        ViewHolder(properties:ViewHolderProperties(borderWidth:0, borderColor:.yellow),
           viewBuilder: {
            RoundedRectangle(cornerRadius: 25.0)
                .fill(Color.blue)
           })
            .edgesIgnoringSafeArea(.all)
        
    }
}
