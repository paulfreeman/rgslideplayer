import SwiftUI


public struct SlidePlayer : View {
    
    @ObservedObject var model: RGSlidePlayerModel
    
    public init(model: RGSlidePlayerModel){
        self.model = model
    }
    
    public var body: some View {
        ZStack{
            
            if model.topViewShowing {
                if let uim = UIImage(contentsOfFile: model.topView){
                    Image(uiImage: uim)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .zIndex(1)
                        .transition(AnyTransition.opacity)
                        .animation(.default)
                        .onAppear {
                            model.prepareForNextCycle()
                        }
                }
                
            }
            
            ///The base view shows the last image and is used to
            ///swap the image
            if let uim = UIImage(contentsOfFile: model.baseView){
                Image(uiImage: uim)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .zIndex(0)
            }
        }
        .frame(width: .infinity, height: .infinity, alignment: .center)
        .edgesIgnoringSafeArea(.all)
    }
}


struct SlidePlayer_Previews: PreviewProvider {
    static var previews: some View {
        SlidePlayer(model: RGSlidePlayerModel(view: "InsertIntoGallery", delegate:nil))
    }
}
