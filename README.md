# RGSlidePlayer

RGSlidePlayer displays a view and transitions it to another view 

These are initially Image views but could be generalised to Views with @viewbuilders ?  

It has to be driven by an external timer or trigger 

To use 

- Set the transition specification  
func setTransition(spec: TransitionSpecification)

- Set the initial view  
func setNext(image :String) 

- Fire the transitions by setting the nextView for each subsequent view  
func setNext(image : String)

 
