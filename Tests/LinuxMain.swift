import XCTest

import RGSlidePlayerTests

var tests = [XCTestCaseEntry]()
tests += RGSlidePlayerTests.allTests()
XCTMain(tests)
